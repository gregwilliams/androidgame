package com.gregwilliams.robotgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.util.Log;
import android.util.SparseArray;

import com.gregwilliams.framework.Screen;
import com.gregwilliams.framework.impl.AndroidGame;

public class SampleGame extends AndroidGame {
    private static SparseArray<String> levelMap = new SparseArray<String>();
    private static int HIGHEST_LEVEL = 2;
    boolean firstTimeCreate = true;

    @Override
    public Screen getInitScreen() {

        if (firstTimeCreate) {
            Assets.load(this);
            firstTimeCreate = false;
        }

        levelMap.put(1, getMapString(R.raw.map1));
        levelMap.put(2, getMapString(R.raw.map2));

        return new SplashLoadingScreen(this);

    }
    
    public static String getMap(int level) {
    	if (level > HIGHEST_LEVEL)
    		level = HIGHEST_LEVEL;
    	return levelMap.get(level);
    }

    @Override
    public void onBackPressed() {
        getCurrentScreen().backButton();
    }
    
    private String getMapString(int id) {
    	InputStream is = getResources().openRawResource(id);
    	return convertStreamToString(is);
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            Log.w("LOG", e.getMessage());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.w("LOG", e.getMessage());
            }
        }
        return sb.toString();
    }

    @Override
    public void onResume() {
        super.onResume();

        Assets.theme.play();

    }

    @Override
    public void onPause() {
        super.onPause();
        Assets.theme.pause();

    }
}
