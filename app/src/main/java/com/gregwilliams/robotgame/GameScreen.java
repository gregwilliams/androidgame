package com.gregwilliams.robotgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import android.app.UiModeManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;

import com.gregwilliams.framework.Game;
import com.gregwilliams.framework.Graphics;
import com.gregwilliams.framework.Input.GamepadEvent;
import com.gregwilliams.framework.Input.TouchEvent;
import com.gregwilliams.framework.Screen;
import com.gregwilliams.framework.impl.AndroidGame;
import com.gregwilliams.framework.impl.Gamepad;

public class GameScreen extends Screen {
	enum GameState {
		Ready, Running, Paused, LevelComplete, GameOver
	}
	static GameState state = GameState.Ready;
	private static final int MAX_ENEMIES = 2;
	
	// Variable Setup
	private static Background bg1, bg2;
	private static Robot robot;
	public static List<Enemy> enemies;

	private Random random = new Random();

	private ArrayList<Tile> tilearray = new ArrayList<Tile>();
	public static int score = 0;
	public static int livesLeft = 1;
	static int level = 1;
	Paint paint, paint2;
	Context applicationContext;

	public GameScreen(Game game) {
		super(game);
        applicationContext = AndroidGame.getApplicationContextInstance();

		// Initialize game objects here
		bg1 = new Background(0, 0);
		bg2 = new Background(2160, 0);
		robot = new Robot();
		enemies = new ArrayList<Enemy>();
		enemies.add(new Heliboy(340, 360));
		enemies.add(new Heliboy(700, 360));

		loadMap();

		// Defining a paint object
		paint = new Paint();
		paint.setTextSize(30);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);

		paint2 = new Paint();
		paint2.setTextSize(100);
		paint2.setTextAlign(Paint.Align.CENTER);
		paint2.setAntiAlias(true);
		paint2.setColor(Color.WHITE);

	}

	private void loadMap() {
		ArrayList<String> lines = new ArrayList<String>();
		int width = 0;
		int height = 0;

		Scanner scanner = new Scanner(SampleGame.getMap(level));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			// no more lines to read
			if (line == null) {
				break;
			}

			if (!line.startsWith("!")) {
				lines.add(line);
				width = Math.max(width, line.length());

			}
		}
		height = lines.size();

		for (int j = 0; j < height; j++) {
			String line = (String) lines.get(j);
			for (int i = 0; i < width; i++) {

				if (i < line.length()) {
					char ch = line.charAt(i);
					Tile t = new Tile(i, j, Character.getNumericValue(ch));
					tilearray.add(t);
				}

			}
		}

	}

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		List<GamepadEvent> gamepadEvents = game.getInput().getGamepadEvents();

		// We have four separate update methods in this example.
		// Depending on the state of the game, we call different update methods.
		// Refer to Unit 3's code. We did a similar thing without separating the
		// update methods.

		if (state == GameState.Ready)
			updateReady(touchEvents, gamepadEvents);
		if (state == GameState.Running)
			updateRunning(touchEvents, gamepadEvents, deltaTime);
		if (state == GameState.Paused)
			updatePaused(touchEvents, gamepadEvents);
		if (state == GameState.GameOver)
			updateGameOver(touchEvents, gamepadEvents);
		if (state == GameState.LevelComplete)
			updateLevelComplete(touchEvents, gamepadEvents);
	}

	private void updateReady(List<TouchEvent> touchEvents, List<GamepadEvent> gamepadEvents) {

		// This example starts with a "Ready" screen.
		// When the user touches the screen, the game begins.
		// state now becomes GameState.Running.
		// Now the updateRunning() method will be called!

		if (touchEvents.size() > 0 || gamepadEvents.size() > 0)
			state = GameState.Running;
	}

	private void updateRunning(List<TouchEvent> touchEvents, List<GamepadEvent> gamepadEvents, float deltaTime) {

		// 1. All input is handled here:
		handleRunningTouchEvents(touchEvents);
        handleRunningGamepadEvents(gamepadEvents);

		// 2. Check miscellaneous events like death:
		if (livesLeft == 0) {
			state = GameState.GameOver;
		}
		
		if (enemies.size() < MAX_ENEMIES) {
			respawnEnemy();
		}
		
		if (robot.getCompletedLevel() == level) {
			levelComplete();
		}

		// 3. Call individual update() methods here.
		// This is where all the game updates happen.
		// For example, robot.update();
		robot.update();
		updateProjectiles();
		updateTiles();
		for (Enemy e : enemies) {
			e.update();
		}
		bg1.update();
		bg2.update();
		animate();

		if (robot.getCenterY() > 500) {
			state = GameState.GameOver;
		}
	}

	private void handleRunningGamepadEvents(List<GamepadEvent> gamepadEvents) {
        for (GamepadEvent event : gamepadEvents) {
            switch (event.type) {
                case GamepadEvent.RIGHT:
                    robot.moveRight();
                    robot.setMovingRight(true);
                    break;
                case GamepadEvent.UP:
                    if (robot.isDucked())
                        robot.setDucked(false);
                    else
                        robot.jump();
                    break;
                case GamepadEvent.DOWN:
                    if (robot.isJumped() == false) {
                        robot.setDucked(true);
                        robot.setSpeedX(0);
                    }
                    break;
                case GamepadEvent.TRIGGER:
                case GamepadEvent.A:
                    if (robot.isDucked() == false && robot.isJumped() == false
                            && robot.isReadyToFire()) {
                        robot.shoot();
                    }
                    break;
                case GamepadEvent.CENTER:
                case GamepadEvent.LEFT:
                    robot.stopRight();
                    break;
                case GamepadEvent.B:
                    pause();
                    break;
            }
        }
    }

	private void handleRunningTouchEvents(List<TouchEvent> touchEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_DOWN) {

				if (inBounds(event, 0, 285, 65, 65)) {
					robot.jump();
					robot.setDucked(false);
				}

				else if (inBounds(event, 0, 350, 65, 65)) {

					if (robot.isDucked() == false && robot.isJumped() == false
							&& robot.isReadyToFire()) {
						robot.shoot();
					}
				}

				else if (inBounds(event, 0, 415, 65, 65)
						&& robot.isJumped() == false) {
					robot.setDucked(true);
					robot.setSpeedX(0);

				}

				if (event.x > 400) {
					// Move right.
					robot.moveRight();
					robot.setMovingRight(true);

				}

			}

			if (event.type == TouchEvent.TOUCH_UP) {

				if (inBounds(event, 0, 415, 65, 65)) {
					robot.setDucked(false);

				}

				if (inBounds(event, 0, 0, 35, 35)) {
					pause();

				}

				if (event.x > 400) {
					// Move right.
					robot.stopRight();
				}
			}

		}
	}

	private void updatePaused(List<TouchEvent> touchEvents, List<GamepadEvent> gamepadEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 0, 0, 800, 240)) {
					if (!inBounds(event, 0, 0, 35, 35)) {
						resume();
					}
				}

				if (inBounds(event, 0, 240, 800, 240)) {
					nullify();
					game.setScreen(new MainMenuScreen(game));
				}
			}
		}

        if (gamepadEvents.size() > 0)
            resume();
	}
	
	private void updateLevelComplete(List<TouchEvent> touchEvents, List<GamepadEvent> gamepadEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_UP) {
				if (inBounds(event, 150, 115, 450, 100)) {
					nullify();
					state = GameState.Ready;
					game.setScreen(new GameScreen(game));
					return;
				} else if (inBounds(event, 150, 310, 450, 100)) {
					nullify();
					state = GameState.Ready;
					game.setScreen(new MainMenuScreen(game));
				}
			}
		}

        for (GamepadEvent event : gamepadEvents) {
            if (event.type == GamepadEvent.A) {
                nullify();
                state = GameState.Ready;
                game.setScreen(new GameScreen(game));
            }
        }
	}

	private void updateGameOver(List<TouchEvent> touchEvents, List<GamepadEvent> gamepadEvents) {
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_DOWN) {
				if (inBounds(event, 0, 0, 800, 480)) {
					newGame();
					game.setScreen(new MainMenuScreen(game));
					state = GameState.Ready;
					return;
				}
			}
		}

        for (GamepadEvent event : gamepadEvents) {
            if (event.type == GamepadEvent.A) {
                newGame();
                game.setScreen(new MainMenuScreen(game));
                state = GameState.Ready;
                return;
            }
        }
	}

	private boolean inBounds(TouchEvent event, int x, int y, int width,
			int height) {
		return (event.x > x && event.x < x + width - 1 && event.y > y
				&& event.y < y + height - 1);
	}

	private void updateProjectiles() {
		ArrayList<Projectile> projectiles = robot.getProjectiles();
		for (int i = 0; i < projectiles.size(); i++) {
			Projectile p = projectiles.get(i);
			if (p.isVisible() == true) {
				p.update();
			} else {
				projectiles.remove(i);
			}
		}
	}

	private void updateTiles() {
		for (int i = 0; i < tilearray.size(); i++) {
			Tile t = (Tile) tilearray.get(i);
			t.update();
		}
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();

		g.drawImage(Assets.background, bg1.getBgX(), bg1.getBgY());
		g.drawImage(Assets.background, bg2.getBgX(), bg2.getBgY());
		paintTiles(g);

		ArrayList<Projectile> projectiles = robot.getProjectiles();
		for (Projectile p : projectiles) {
			g.drawRect(p.getX(), p.getY(), 10, 5, Color.YELLOW);
		}

		// First draw the game elements.
		robot.draw(g);
		drawEnemies(g);
		g.drawString(Integer.toString(score), 740, 30, paint);

		// Secondly, draw the UI above the game elements.
		if (state == GameState.Ready)
			drawReadyUI();
		if (state == GameState.Running)
			drawRunningUI();
		if (state == GameState.Paused)
			drawPausedUI();
		if (state == GameState.GameOver)
			drawGameOverUI();
		if (state == GameState.LevelComplete)
			drawLevelUpUI();

	}

	private void drawEnemies(Graphics g) {
		for (int i=0; i<enemies.size(); i++) {
			Enemy e = enemies.get(i);
			e.draw(g);
			if(e.isDead()) {
				enemies.remove(i);
			}
		}
	}

	private void respawnEnemy() {
		if (random.nextInt(100 - level) == 0) {
			enemies.add(new Heliboy(800, 360));
		}
	}

	private void paintTiles(Graphics g) {
		for (int i = 0; i < tilearray.size(); i++) {
			Tile t = (Tile) tilearray.get(i);
			if (t.type != 0) {
				g.drawImage(t.getTileImage(), t.getTileX(), t.getTileY());
			}
		}
	}

	public void animate() {
		robot.animate();
		for (Enemy e : enemies)
			e.animate();
	}

	private void nullify() {
		// Set all variables to null. You will be recreating them in the
		// constructor.
		paint = null;
		bg1 = null;
		bg2 = null;
		robot = null;
		enemies = new ArrayList<Enemy>();

		// Call garbage collector to clean up memory.
		System.gc();

	}
	
	private void newGame() {
		nullify();
		level = 1;
		score = 0;
	}

	private void drawReadyUI() {
		Graphics g = game.getGraphics();

		g.drawARGB(155, 0, 0, 0);

        // set ready menu strings depending on if we're on Android TV or phone/tablet
        if (((UiModeManager) AndroidGame.getApplicationContextInstance().getSystemService(
                Context.UI_MODE_SERVICE)).getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION)
		    g.drawString("Press A to Start.", 400, 240, paint);
        else
		    g.drawString("Tap to Start.", 400, 240, paint);

	}

	private void drawRunningUI() {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.button, 0, 285, 0, 0, 65, 65);
		g.drawImage(Assets.button, 0, 350, 0, 65, 65, 65);
		g.drawImage(Assets.button, 0, 415, 0, 130, 65, 65);
		g.drawImage(Assets.button, 0, 0, 0, 195, 35, 35);

	}

	private void drawPausedUI() {
		Graphics g = game.getGraphics();
		// Darken the entire screen so you can display the Paused screen.
		g.drawARGB(155, 0, 0, 0);
		g.drawString("Resume", 400, 165, paint2);
		g.drawString("Menu", 400, 360, paint2);

	}

	private void drawLevelUpUI() {
		Graphics g = game.getGraphics();
		// Darken the entire screen so you can display the Level Up screen.
		g.drawARGB(155, 0, 0, 0);
		g.drawString("Level " + level, 400, 165, paint2);
		g.drawString("Menu", 400, 360, paint2);

	}

	private void drawGameOverUI() {
		Graphics g = game.getGraphics();
		g.drawRect(0, 0, 1281, 801, Color.BLACK);
		g.drawString("GAME OVER.", 400, 240, paint2);

        // set game over strings depending on if we're on Android TV or phone/tablet
		g.drawString(applicationContext.getString(R.string.return_prompt), 400, 290, paint);

	}

	@Override
	public void pause() {
		if (state == GameState.Running)
			state = GameState.Paused;

	}

	@Override
	public void resume() {
		if (state == GameState.Paused)
			state = GameState.Running;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		pause();
	}

	public static Background getBg1() {
		return bg1;
	}

	public static Background getBg2() {
		return bg2;
	}

	public static Robot getRobot() {
		return robot;
	}

	public void levelComplete() {
		if (state == GameState.Running) {
			state = GameState.LevelComplete;
			level++;
		}
	}
}
