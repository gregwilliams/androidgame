package com.gregwilliams.robotgame;

import android.graphics.Rect;

import com.gregwilliams.framework.Graphics;

public abstract class Enemy {
    protected int power, centerX, speedX, centerY;
    protected Background bg = GameScreen.getBg1();
    protected Robot robot = GameScreen.getRobot();
    protected boolean dead = false;

    public Rect r = new Rect(0, 0, 0, 0);
    public int health = 5;

    private int movementSpeed;

    // Behavioral Methods
    public void update() {
        follow();
        centerX += speedX;
        speedX = bg.getSpeedX() * 5 + movementSpeed;
        setCollisionRect();

        if (Rect.intersects(r, robot.yellowRed)) {
            checkCollision();
        }
    }
    
    protected abstract void setCollisionRect();

	private void checkCollision() {
		if ((!robot.isDucked())
				&& (Rect.intersects(r, robot.rect)
						|| Rect.intersects(r, robot.rect2)
						|| Rect.intersects(r, robot.rect3) || Rect.intersects(
						r, robot.rect4))) {
			GameScreen.livesLeft--;
		}
	}

    public void follow() {
        
        if (centerX < -95 || centerX > 810){
            movementSpeed = 0;
            die();
        }

        else if (Math.abs(robot.getCenterX() - centerX) < 5) {
            movementSpeed = 0;
        }

        else {

            if (robot.getCenterX() >= centerX) {
                movementSpeed = 1;
            } else {
                movementSpeed = -1;
            }
        }

    }

    public void die() {
    	setCenterX(-100);
    	dead = true;
    }

    public boolean isDead() {
		return dead;
	}

	public void attack() {

    }

    public int getPower() {
        return power;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public Background getBg() {
        return bg;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public void setBg(Background bg) {
        this.bg = bg;
    }

    public abstract void draw(Graphics g);
    
    public abstract void animate();
}
