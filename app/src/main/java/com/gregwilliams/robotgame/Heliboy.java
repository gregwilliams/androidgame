package com.gregwilliams.robotgame;

import com.gregwilliams.framework.Graphics;
import com.gregwilliams.framework.Image;

public class Heliboy extends Enemy {
	private Image heliboy, heliboy2, heliboy3, heliboy4, heliboy5;
	private Animation hanim;
	
    public Heliboy(int centerX, int centerY) {
    	health = 5;
        setCenterX(centerX);
        setCenterY(centerY);
        init();
    }
    
    private void init()
    {
    	heliboy = Assets.heliboy;
		heliboy2 = Assets.heliboy2;
		heliboy3 = Assets.heliboy3;
		heliboy4 = Assets.heliboy4;
		heliboy5 = Assets.heliboy5;

		hanim = new Animation();
		hanim.addFrame(heliboy, 100);
		hanim.addFrame(heliboy2, 100);
		hanim.addFrame(heliboy3, 100);
		hanim.addFrame(heliboy4, 100);
		hanim.addFrame(heliboy5, 100);
		hanim.addFrame(heliboy4, 100);
		hanim.addFrame(heliboy3, 100);
		hanim.addFrame(heliboy2, 100);
    }

	@Override
    public void animate() {
		hanim.update(50);
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(hanim.getImage(), getCenterX() - 48, getCenterY() - 48);
	}

	@Override
	protected void setCollisionRect() {
		r.set(centerX - 25, centerY - 25, centerX + 25, centerY + 35);
	}

}
