package com.gregwilliams.framework.impl;

import java.util.List;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;

import com.gregwilliams.framework.Input;

public class AndroidInput implements Input {
    TouchHandler touchHandler;
    GamepadHandler gamePadHandler;

    public AndroidInput(Context context, View view, float scaleX, float scaleY) {
        if(VERSION.SDK_INT < 5) 
            touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
        else
            touchHandler = new MultiTouchHandler(view, scaleX, scaleY);

        gamePadHandler = new GamepadHandler(view);
    }


    @Override
    public boolean isTouchDown(int pointer) {
        return touchHandler.isTouchDown(pointer);
    }

    @Override
    public int getTouchX(int pointer) {
        return touchHandler.getTouchX(pointer);
    }

    @Override
    public int getTouchY(int pointer) {
        return touchHandler.getTouchY(pointer);
    }

    @Override
    public List<TouchEvent> getTouchEvents() {
        return touchHandler.getTouchEvents();
    }

    @Override
    public List<GamepadEvent> getGamepadEvents() { return gamePadHandler.getGamepadEvents(); }
}
