package com.gregwilliams.framework.impl;

import android.view.InputDevice;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.gregwilliams.framework.Input.GamepadEvent;

public class Gamepad {
    int buttonPressed = -1; // initialized to -1

    public int getButtonPressed(InputEvent event) {
        if (!isGamepadDevice(event)) {
            return -1;
        }

        // If the input event is a MotionEvent, check its hat axis values.
        if (event instanceof MotionEvent) {

            // Use the hat axis value to find the D-pad direction
            MotionEvent motionEvent = (MotionEvent) event;
            float xaxis = motionEvent.getAxisValue(MotionEvent.AXIS_HAT_X);
            float yaxis = motionEvent.getAxisValue(MotionEvent.AXIS_HAT_Y);
            float zaxis = motionEvent.getAxisValue(MotionEvent.AXIS_RTRIGGER);

            // Check if the AXIS_HAT_X value is -1 or 1, and set the D-pad
            // LEFT and RIGHT direction accordingly.
            if (Float.compare(xaxis, -1.0f) == 0) {
                buttonPressed =  GamepadEvent.LEFT;
            } else if (Float.compare(xaxis, 1.0f) == 0) {
                buttonPressed =  GamepadEvent.RIGHT;
            }
            // Check if the AXIS_HAT_Y value is -1 or 1, and set the D-pad
            // UP and DOWN direction accordingly.
            else if (Float.compare(yaxis, -1.0f) == 0) {
                buttonPressed =  GamepadEvent.UP;
            } else if (Float.compare(yaxis, 1.0f) == 0) {
                buttonPressed =  GamepadEvent.DOWN;
            }
            // Check for trigger
            else if (Float.compare(zaxis, 1.0f) == 0) {
                buttonPressed = GamepadEvent.TRIGGER;
            }
        }

        // If the input event is a KeyEvent, check its key code.
        else if (event instanceof KeyEvent) {

            // Use the key code to find the D-pad direction.
            KeyEvent keyEvent = (KeyEvent) event;
            if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT) {
                buttonPressed = GamepadEvent.LEFT;
            } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT) {
                buttonPressed = GamepadEvent.RIGHT;
            } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP) {
                buttonPressed = GamepadEvent.UP;
            } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN) {
                buttonPressed = GamepadEvent.DOWN;
            } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER) {
                buttonPressed = GamepadEvent.CENTER;
            } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BUTTON_A) {
                buttonPressed = GamepadEvent.A;
            } else if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BUTTON_B) {
                buttonPressed = GamepadEvent.B;
            }
        }
        return buttonPressed;
    }

    public static boolean isGamepadDevice(InputEvent event) {
        // Check that input comes from a device with directional pads.
        if ((event.getSource() & InputDevice.SOURCE_DPAD)
                != InputDevice.SOURCE_DPAD) {
            return true;
        } else  if ((event.getSource() & InputDevice.SOURCE_GAMEPAD)
                != InputDevice.SOURCE_GAMEPAD) {
            return true;
        } else {
            return false;
        }
    }
}