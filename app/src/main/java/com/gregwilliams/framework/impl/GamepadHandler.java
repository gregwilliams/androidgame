package com.gregwilliams.framework.impl;

import android.util.Log;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import com.gregwilliams.framework.Input.GamepadEvent;
import com.gregwilliams.framework.Pool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by greg on 5/16/15.
 */
public class GamepadHandler implements View.OnGenericMotionListener, View.OnKeyListener
{
    static final String LOG_TAG = GamepadHandler.class.getSimpleName();
    final Pool<GamepadEvent> gamepadEventPool;
    List<GamepadEvent> gamepadEvents = new ArrayList<GamepadEvent>();
    List<GamepadEvent> gamepadEventsBuffer = new ArrayList<GamepadEvent>();
    Gamepad gamepad;

    public GamepadHandler(View view)
    {
        Pool.PoolObjectFactory<GamepadEvent> factory = new Pool.PoolObjectFactory<GamepadEvent>() {
            @Override
            public GamepadEvent createObject() {
                return new GamepadEvent();
            }
        };
        gamepad = new Gamepad();
        gamepadEventPool = new Pool<GamepadEvent>(factory, 100);
        view.setOnGenericMotionListener(this);
        Log.v(LOG_TAG, "Setting motion listener");
    }

    @Override
    public boolean onGenericMotion(View v, MotionEvent event) {
        return handleInput(event);
    }

    private void addToBuffer(int type) {
        GamepadEvent event = gamepadEventPool.newObject();
        event.type = type;
        gamepadEventsBuffer.add(event);
    }

    public List<GamepadEvent> getGamepadEvents() {
        synchronized (this) {
            int len = gamepadEvents.size();
            for (int i = 0; i < len; i++)
                gamepadEventPool.free(gamepadEvents.get(i));
            gamepadEvents.clear();
            gamepadEvents.addAll(gamepadEventsBuffer);
            gamepadEventsBuffer.clear();
            return gamepadEvents;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return handleInput(event);
    }

    private boolean handleInput(InputEvent event) {
        synchronized (this) {
            int buttonType = gamepad.getButtonPressed(event);
            Log.v(LOG_TAG, "Event " + buttonType + " pressed");
            if (Gamepad.isGamepadDevice(event)) {
                if (buttonType > -1) {
                    addToBuffer(buttonType);
                    return true;
                }
            }
        }
        return false;
    }
}
