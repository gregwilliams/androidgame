package com.gregwilliams.framework;

import java.util.List;

public interface Input {
    public static class TouchEvent {
        public static final int TOUCH_DOWN = 0;
        public static final int TOUCH_UP = 1;
        public static final int TOUCH_DRAGGED = 2;
        public static final int TOUCH_HOLD = 3;

        public int type;
        public int x, y;
        public int pointer;
    }

    public static class GamepadEvent {
        public final static int UP       = 0;
        public final static int LEFT     = 1;
        public final static int RIGHT    = 2;
        public final static int DOWN     = 3;
        public final static int CENTER   = 4;
        public final static int A        = 5;
        public final static int B        = 6;
        public final static int TRIGGER  = 7;

        public int type;
    }

    public boolean isTouchDown(int pointer);

    public int getTouchX(int pointer);

    public int getTouchY(int pointer);

    public List<TouchEvent> getTouchEvents();

    public List<GamepadEvent> getGamepadEvents();
}
