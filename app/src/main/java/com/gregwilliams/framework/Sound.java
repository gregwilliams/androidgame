package com.gregwilliams.framework;

public interface Sound {
    public void play(float volume);

    public void dispose();
}
